import baseURL from '@/static/baseURL'
import Cookies from 'universal-cookie'  

const cookies = new Cookies()

const state = {
    DT: [],
    error: null,
    isLoading: false
};

const mutations = {
    UPD_DT(state, data) {
        state.DT = data
        state.error = null
        state.isLoading = false
    },
    SUCCESS(state) {
        state.isLoading = false
    },
    LOADING(state) {
        state.isLoading = true
    },
    ERROR(state, error) {
        state.error = error
    },
    RESETERROR(state){
        state.error = null
    }
}; 

const actions = {
    getDataList({ commit }, params) {
        return new Promise(async (resolve, reject) => {
            commit('TOGGLE_LOADING', null, { root: true })
            try {
                const config = { params }
                const {data} = await baseURL.get('api/settings/clients/json', config)  
                commit('UPD_DT', data);
                commit('TOGGLE_LOADING', null, { root: true })
                resolve(data)
            } catch(err){
                commit('TOGGLE_LOADING', null, { root: true })
                commit('ERROR', err.response.data.message)
                reject()
            }
        })
    }, 
}

export default {
    namespaced: true,
    state,
    mutations,
    actions, 
}